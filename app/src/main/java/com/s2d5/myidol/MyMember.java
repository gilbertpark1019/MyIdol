package com.s2d5.myidol;

/**
 * Created by gilbert-mac on 2017. 7. 1..
 */

public class MyMember {
    int image;
    String name;
    String description;

    public MyMember(int image, String name, String description) {
        this.image = image;
        this.name = name;
        this.description = description;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
