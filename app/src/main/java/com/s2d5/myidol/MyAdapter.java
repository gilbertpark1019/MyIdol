package com.s2d5.myidol;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by gilbert-mac on 2017. 7. 1..
 */

public class MyAdapter extends BaseAdapter {
    Context context;
    ArrayList<MyMember> datas;

    public MyAdapter(Context context, ArrayList<MyMember> datas) {
        this.context = context;
        this.datas = datas;
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if(convertView==null) {
            LayoutInflater mInflater =
                    (LayoutInflater)context.getSystemService
                            (Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item, parent, false);
            holder = new Holder();
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.image =(ImageView)convertView.findViewById(R.id.item_image);
        holder.name =(TextView)convertView.findViewById(R.id.item_name);
        holder.description =(TextView) convertView.findViewById(R.id.item_description);

        holder.image.setImageResource(datas.get(position).getImage());
        holder.name.setText(datas.get(position).getName());
        holder.description.setText(datas.get(position).getDescription());

        return convertView;
    }

    public class Holder{
        public ImageView image;
        public TextView name;
        public TextView description;
    }
}
