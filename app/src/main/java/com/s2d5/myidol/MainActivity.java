package com.s2d5.myidol;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    LinearLayout root;
    AppCompatSpinner spinner;
    ListView listview;
    ArrayList<MyMember> Apink = new ArrayList<>();
    MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SetApink();

        root = (LinearLayout) findViewById(R.id.root);
        spinner = (AppCompatSpinner) findViewById(R.id.spinner);
        listview = (ListView) findViewById(R.id.listview);
        spinner.setOnItemSelectedListener(this);
    }

    private void SetApink() {
        String[] apinkArray = getResources().getStringArray(R.array.Apink);
        Apink.add(new MyMember(R.mipmap.apink_01chorong, apinkArray[0].split("@")[0],apinkArray[0].split("@")[1]));
        Apink.add(new MyMember(R.mipmap.apink_02bomi, apinkArray[1].split("@")[0],apinkArray[1].split("@")[1]));
        Apink.add(new MyMember(R.mipmap.apink_03eunji, apinkArray[2].split("@")[0],apinkArray[2].split("@")[1]));
        Apink.add(new MyMember(R.mipmap.apink_04naeun, apinkArray[3].split("@")[0],apinkArray[3].split("@")[1]));
        Apink.add(new MyMember(R.mipmap.apink_05namju, apinkArray[4].split("@")[0],apinkArray[4].split("@")[1]));
        Apink.add(new MyMember(R.mipmap.apink_06hayoung, apinkArray[5].split("@")[0],apinkArray[5].split("@")[1]));
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                root.setBackgroundResource(R.mipmap.apink);
                adapter = new MyAdapter(MainActivity.this, Apink);
                listview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                break;
            default:
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
